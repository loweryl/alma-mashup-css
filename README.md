# README #

### Alma Mashup CSS - for use with Ex Libris Alma ###

The files in this repository are CSS stylesheets used by Western Washington University.  The stylesheets control the formatting of the Alma "View it" and "Find it" windows in WWU's Primo front end, as well as the Summit (Orbis Cascade Alliance consortial borrowing program) and local request forms.

### What's in this repository? ###

* skin/branding_skin: WWU's template mashup files for upload to Alma Delivery System Skins interface
* local_request_form.css: Styles used by WWU to format the local (WWU) request form
* summit_request_form.css: Styles used by WWU to format the Summit request form
* wwu_findit.css: Styles used by WWU to format the Find It window
* wwu_viewit.css: Styles used by WWU to format the View It window

### How do I get set up? ###

To use these stylesheets, you'll need to download your mashup.css file from Alma.  Go to Alma --> General Configuration --> Configuration Menu --> Branding/Logo --> Delivery System Skins.  Choose the skin you'd like to edit, and click on the "Skin Zip File" link to download the mashup files.  Extract the files to a location of your choice, and open the mashup.css file for editing.

**If you keep your CSS in the mashup file**: copy the contents of the desired stylesheet(s) in this repository and use them to replace your existing request form styling in mashup.css.  

**If you host your CSS on a local web server**: Save the desired stylesheet(s) in this repository to your local web server.  Open mashup.css and add the following line for each file:

```
#!CSS

@import url("//path/to/css/files/filename.css");
```


For example, WWU's premium sandbox Find It tab uses a mashup.css that consists of these three lines:

```
#!CSS

@import url("//dev1.library.wwu.edu/alma/mashup_css/wwu_findit.css");
@import url("//dev1.library.wwu.edu/alma/mashup_css/summit_request_form.css"); 
@import url("//dev1.library.wwu.edu/alma/mashup_css/local_request_form.css"); 
```

**For both options**: save mashup.css and re-zip the branding skin into a file named skin.zip.  Upload the updated skin.zip in Alma (on the same page where you downloaded it in the prior step).

Your changes should be immediately visible in the Primo front end.

### Who do I talk to? ###

* Lesley Lowery, WWU:  [loweryl@wwu.edu](mailto:loweryl@wwu.edu)